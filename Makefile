build:
	gcc lib/libzeitgeist-2.0.c -o libzeitgeist-2.0.so -shared

makedirs:
	mkdir -p $(DESTDIR)/usr/bin/
	mkdir -p $(DESTDIR)/usr/lib/
	mkdir -p $(DESTDIR)/usr/share/dbus-1/services
	mkdir -p $(DESTDIR)/usr/share/vala/vapi/
	mkdir -p $(DESTDIR)/usr/share/gir-1.0/
	mkdir -p $(DESTDIR)/usr/include/
	mkdir -p $(DESTDIR)/usr/lib/pkgconfig

install: makedirs
	install bin/zeitgeist-daemon $(DESTDIR)/usr/bin/zeitgeist-daemon
	install libzeitgeist-2.0.so $(DESTDIR)/usr/lib/libzeitgeist-2.0.so
	ln -s ./libzeitgeist-2.0.so $(DESTDIR)/usr/lib/libzeitgeist-2.0.so.0.0
	ln -s ./libzeitgeist-2.0.so $(DESTDIR)/usr/lib/libzeitgeist-2.0.so.0
	cp -prf dbus/* $(DESTDIR)/usr/share/dbus-1/services
	cp -prf vapi/* $(DESTDIR)/usr/share/vala/vapi/
	cp -prf gir/* $(DESTDIR)/usr/share/gir-1.0/
	cp -prf header/* $(DESTDIR)/usr/include/
	cp -prf pkgconfig/* $(DESTDIR)/usr/lib/pkgconfig/

clean:
	rm -f libzeitgeist-2.0.so
